package com.worldremit.sousers.userdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.squareup.picasso.Picasso;
import com.worldremit.sousers.R;
import com.worldremit.sousers.api.model.User;
import com.worldremit.sousers.common.BaseMvpActivity;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDetailActivity extends BaseMvpActivity implements UserDetailPresenter.UserDetailView {

    private UserDetailPresenter userDetailPresenter;

    @BindView(R.id.title_bk_actionbar_title_centre)
    public AppCompatTextView mActionBarTitleTv;

    @BindView(R.id.item_detail_iv)
    public ImageView mItemIv;

    @BindView(R.id.followingTv)
    public AppCompatTextView mFollowingTv;

    @BindView(R.id.body_item_1_tv)
    public AppCompatTextView mUserIdTv;

    @BindView(R.id.body_item_2_tv)
    public AppCompatTextView mReputationTv;

    @BindView(R.id.body_item_3_tv)
    public AppCompatTextView mLocationTv;

    @BindView(R.id.body_item_4_tv)
    public AppCompatTextView mMembershipSince;

    @BindView(R.id.title_bk_actionbar_btn_start)
    public View mBackBtnIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_detail);

        ButterKnife.bind(this);

        userDetailPresenter = new UserDetailPresenter(getIntent().getParcelableExtra("USER_ID"));
        userDetailPresenter.onCreate(this);
        userDetailPresenter.onStart();
    }


    @Override
    public void showUser(User user) {
        try {
            if(user.isFollowed()) mFollowingTv.setVisibility(View.VISIBLE);

            mActionBarTitleTv.setText(user.getDisplayName());

            Picasso.get().load(user.getProfileImage()).into(mItemIv);

            mUserIdTv.setText(getString(R.string.userId_string_fmt, user.getUserId()));
            mReputationTv.setText(getString(R.string.reputation_string_fmt, user.getReputation()));
            mLocationTv.setText(getString(R.string.location_string_fmt, user.getLocation()));

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(user.getCreationDate());

            // TODO: Update Utils to display this in the required format
            mMembershipSince.setText(getString(R.string.membership_since_string_fmt, cal.getTime().toString()));
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.title_bk_actionbar_btn_start)
    public void backBtnClick(){
        onBackPressed();
    }
}
