package com.worldremit.sousers.userdetail;

import com.worldremit.sousers.api.model.User;
import com.worldremit.sousers.common.BaseMvpPresenter;
import com.worldremit.sousers.common.BaseMvpView;

import org.jetbrains.annotations.NotNull;

public class UserDetailPresenter extends BaseMvpPresenter {

    private UserDetailView view;
    private User mUser;

    interface UserDetailView extends BaseMvpView {
        void showUser(User user);
    }

    @Override
    public void onCreate(@NotNull BaseMvpView view) {
        this.view = (UserDetailView)view;
    }

    public UserDetailPresenter(User user) {
        mUser = user;
    }

    @Override
    public void onStart() {
        view.showUser(mUser);
    }
}
