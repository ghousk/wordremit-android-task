package com.worldremit.sousers.userlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.worldremit.sousers.R;
import com.worldremit.sousers.api.model.User;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

class UsersAdapter extends RecyclerView.Adapter {

    public UsersAdapter(Context context){
        mContext = context;
    }
    private PublishSubject<User> clickSubject = PublishSubject.create();
    public Observable<User> clickEvent  = clickSubject;
    private Context mContext;
    private List<User> users = Collections.emptyList();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new StackOverflowUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((StackOverflowUserViewHolder)holder).bind(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    class StackOverflowUserViewHolder extends RecyclerView.ViewHolder {

        StackOverflowUserViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(User user) {
            TextView    itemName    =   itemView.findViewById(R.id.item_name);
            TextView    itemId      =   itemView.findViewById(R.id.item_id);
            ImageView   itemImage   =   itemView.findViewById(R.id.item_iv);
            Button      item_btn_follow =   itemView.findViewById(R.id.item_btn_follow);
            Button      item_btn_block  =   itemView.findViewById(R.id.item_btn_block);
            View        itemLayout      =   itemView.findViewById(R.id.item_user_layout);

            try {
                itemName.setText(user.getDisplayName());
                itemId.setText(user.getUserId().toString());
                Picasso.get().load(user.getProfileImage()).placeholder(R.drawable.ic_launcher_foreground).into(itemImage);
                item_btn_block.setOnClickListener(view -> {
                    user.setBlocked(!user.isBlocked());
                    item_btn_block.setText(user.isBlocked()? R.string.unblock : R.string.block);
                    itemLayout.setBackgroundColor(user.isBlocked()?
                            mContext.getColor(R.color.color_background_primary2) :
                            mContext.getColor(R.color.color_background_primary1));
                    if(user.isBlocked()) {
                        user.setFollowed(false);
                        item_btn_follow.setText(R.string.follow);
                    }
                    //mDatabase.userDao().save(user);
                });

                item_btn_follow.setOnClickListener(view -> {
                    if(user.isBlocked()) return;

                    user.setFollowed(!user.isFollowed());
                    item_btn_follow.setText(user.isFollowed()? R.string.unfollow : R.string.follow);
                    //mDatabase.userDao().save(user);
                });

                itemLayout.setOnClickListener(view -> clickSubject.onNext(user));
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
