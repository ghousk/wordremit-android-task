package com.worldremit.sousers.userlist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.worldremit.sousers.App;
import com.worldremit.sousers.R;
import com.worldremit.sousers.api.model.User;
import com.worldremit.sousers.common.BaseMvpActivity;
import com.worldremit.sousers.userdetail.UserDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseMvpActivity implements UserListPresenter.UserListView {

    private UserListPresenter userListPresenter;
    private UsersAdapter adapter;

    @BindView(R.id.home_actionbar_title_centre)
    public AppCompatTextView mActionBarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_list);

        ButterKnife.bind(this);


        mActionBarTitle.setText(R.string.app_name);
        App app = (App) getApplication();
        userListPresenter = new UserListPresenter(app.getUsersRepository());
        userListPresenter.onCreate(this);
        userListPresenter.onStart();


        setupRecyclerView();
    }

    private void setupRecyclerView() {
        adapter = new UsersAdapter(this);
        RecyclerView list = findViewById(R.id.users_list);
        list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(this));
        adapter.clickEvent.subscribe(user -> showDetailedActivity(user));
    }

    private void showDetailedActivity(User user) {
        if(user.isBlocked()) return;

        Intent intent = new Intent(this, UserDetailActivity.class);
        intent.putExtra("USER_ID", user);
        startActivity(intent);
    }

    @Override
    public void showUsers(List<User> users) {
        adapter.setUsers(users);
    }
}
